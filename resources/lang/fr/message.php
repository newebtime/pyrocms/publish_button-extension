<?php

return [
    'publish_success'   => ':count ligne(s) ont été publié avec succès.',
    'unpublish_success' => ':count ligne(s) ont été dépublié avec succès.',
];
