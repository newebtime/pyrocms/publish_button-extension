<?php

return [
    'publish_success'   => ':count row(s) were published successfully.',
    'unpublish_success' => ':count row(s) were unpublished successfully.',
];
