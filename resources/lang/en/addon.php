<?php

return [
    'title'       => 'Publish Button',
    'name'        => 'Publish Button Extension',
    'description' => 'Publish and Unpublish buttons for TableBuilder'
];
