# Publish Button Extension

Publish and Unpublish buttons for TableBuilder

## Installation

```bash
composer require newebtime/publish_button-extension
```

For more details: https://pyrocms.com/documentation/pyrocms/3.4/installation/installing-addons

## How to use?

First you will need to add the `Publishable` trait to your Model.

```php
use Newebtime\PublishButtonExtension\Traits\Publishable;

class PageModel extends PagesPagesEntryModel implements PageInterface
{
    use Publishable;
    
    // ...
}
```

Then you can simple add the button in the `TableBuilder`

```php
//...
    protected $actions = [
        'delete',
        'publish',
        'unpublish',
    ];
//...
```

## Customize

By default it will use the field `enabled` to check the status. You can change it in the model.

```php
    //...
    use Publishable;
    
    protected $publishField = 'published'
    // ...
}
```
