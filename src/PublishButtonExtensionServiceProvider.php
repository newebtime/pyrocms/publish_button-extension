<?php

namespace Newebtime\PublishButtonExtension;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Ui\Table\Component\Action\ActionRegistry;
use Newebtime\PublishButtonExtension\Handler\Publish;
use Newebtime\PublishButtonExtension\Handler\Unpublish;

/**
 * Class PublishButtonExtensionServiceProvider
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PublishButtonExtensionServiceProvider extends AddonServiceProvider
{
    /**
     * {@inheritdoc}
     */
    protected $singletons = [
        ActionRegistry::class => ActionRegistry::class
    ];

    public function register()
    {
        app(ActionRegistry::class)->register('publish', [
            'text'    => 'newebtime.extension.publish_button::button.publish',
            'button'  => 'info',
            'icon'    => 'fa fa-check',
            'handler' => Publish::class,
        ]);

        app(ActionRegistry::class)->register('unpublish', [
            'text'    => 'newebtime.extension.publish_button::button.unpublish',
            'button'  => 'warning',
            'icon'    => 'fa fa-times',
            'handler' => Unpublish::class,
        ]);
    }
}
