<?php

namespace Newebtime\PublishButtonExtension\Traits;

/**
 * Trait Publishable
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
trait Publishable
{
    public function publish($publish = true)
    {
        $field = $this->publishField ?? 'enabled';

        $this->$field = $publish;

        $this->save();
    }
}
