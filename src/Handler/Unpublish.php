<?php

namespace Newebtime\PublishButtonExtension\Handler;

use Anomaly\Streams\Platform\Model\EloquentModel;
use Anomaly\Streams\Platform\Ui\Table\Component\Action\ActionHandler;
use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

/**
 * Class Unpublish
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class Unpublish extends ActionHandler
{
    /**
     * Delete the selected entries.
     *
     * @param TableBuilder $builder
     * @param array        $selected
     */
    public function handle(TableBuilder $builder, array $selected)
    {
        $count = 0;

        $model = $builder->getTableModel();

        if (!method_exists($model, 'publish')) {
            return;
        }

        /* @var EloquentModel $entry */
        foreach ($selected as $id) {
            $entry = $model->find($id);
            $entry->publish(false);

            $builder->fire('row_published', compact('builder', 'model', 'entry'));

            $count++;
        }

        if ($count) {
            $builder->fire('rows_published', compact('count', 'builder', 'model'));
        }

        if ($selected) {
            $this->messages->success(
                trans('newebtime.extension.publish_button::message.unpublish_success', compact('count'))
            );
        }
    }
}
