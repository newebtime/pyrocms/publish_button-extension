<?php

namespace Newebtime\PublishButtonExtension;

use Anomaly\Streams\Platform\Addon\Extension\Extension;

/**
 * Class PublishButtonExtension
 *
 * @link   https://www.newebtime.com/
 * @author Newebtime Co., Ltd. <support@newebtime.com>
 * @author Frédéric Vandebeuque <fred.vdb@newebtime.com>
 */
class PublishButtonExtension extends Extension
{

}
